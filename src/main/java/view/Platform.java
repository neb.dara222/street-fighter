package view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import model.*;
import model.Character;

import java.util.ArrayList;

public class Platform extends Pane {

    public static final int WIDTH = 1500; //960;
    public static final int HEIGHT = 600;
    public static final int GROUND = 300;

    private Image platformImg;

    private Character mainCharacter;
    private Character2 anotherCharacter;
    private ArrayList<Enemy> enemyList;
    private Score score;
    private LifeBar lifeBar;
    private LifeBar lifeBar2;
    private FighterImage fighterImage;
    private FighterImage fighterImage2;

    private Keys keys;

    public Platform() {
        enemyList = new ArrayList();
        keys = new Keys();
        platformImg = new Image(getClass().getResourceAsStream("/background.gif"));
        ImageView backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        mainCharacter = new Character(50, GROUND+((HEIGHT-GROUND)/2)-50);
        anotherCharacter = new Character2(1300,GROUND+((HEIGHT-GROUND)/2)-50);

        buildEnemyList();
        score = new Score(50,HEIGHT - 50);
        lifeBar = new LifeBar(250,10);
        lifeBar2 = new LifeBar(950,10);

        fighterImage = new FighterImage(0,0,new Image(getClass().getResourceAsStream("/Ken_face.gif")));
        fighterImage2 = new FighterImage(1330,0, new Image(getClass().getResourceAsStream("/akuma.png")));

        getChildren().add(backgroundImg);
        getChildren().addAll(enemyList);
        getChildren().add(score);
        getChildren().add(lifeBar);
        getChildren().add(lifeBar2);
        getChildren().add(fighterImage);
        getChildren().add(fighterImage2);
        getChildren().add(mainCharacter);
        getChildren().add(anotherCharacter);
    }

    private void buildEnemyList() {
        for (int i=0 ; i<5 ; i++) {
            Enemy enemy = new Enemy(mainCharacter);
            enemyList.add(enemy);
        }
    }

    public Character getMainCharacter() { return mainCharacter; }

    public Character2 getAnotherCharacter() {return anotherCharacter;}

    public ArrayList<Enemy> getEnemyList() { return enemyList; }

    public Keys getKeys() { return keys; }

    public Score getScore() {return score;}

    public LifeBar getLifeBar() { return  lifeBar;}

}

