package model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class FighterImage extends Pane {

    private Image fighterImg;
    private ImageView fighterImageView;

    public FighterImage(int x, int y, Image image){
        setTranslateX(x);
        setTranslateY(y);
        this.fighterImg = image;
        this.fighterImageView = new ImageView(fighterImg);
        this.fighterImageView.setFitWidth(150);
        this.fighterImageView.setFitHeight(200);
        getChildren().add(fighterImageView);
    }
}
