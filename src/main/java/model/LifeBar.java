package model;

import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Pane;

public class LifeBar extends Pane {
    ProgressBar pbar;
    public LifeBar(int x, int y) {
        setTranslateX(x);
        setTranslateY(y);
        pbar = new ProgressBar();
        pbar.setProgress(100);
        pbar.setScaleX(5);
        pbar.setMaxSize(100,30);
        pbar.setTranslateX(100);
        getChildren().add(pbar);
    }

    public void setLifeBar(double lifePercentage){
        this.pbar.setProgress(lifePercentage);
    }
}
